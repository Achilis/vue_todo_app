import Login from './components/login.vue';
import SignUp from './components/signup.vue';
import Welcome from './components/welcome.vue';
import Admin from './components/Admin.vue';
import Event from './components/Events.vue';
import AddTodo from './components/AddTodo.vue';
import About from './components/About.vue';
export const routes = [
    {path:'/', component:Welcome},
    {path:'/login', component:Login},
    {path:'/signup', component:SignUp},
    {path:'/admin', component:Admin},
    {path:'/events', component:Event},
    {path:'/addTodo', component:AddTodo},
    {path:'/about', component:About},
]