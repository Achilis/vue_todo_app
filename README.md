# vuejs-playlist
                                            **home page**
![](home.jpg)

                                            **login page**
![](login.jpg)

                                            **signup page**
![](signup.jpg)

                                            **addtodo page**
![](addtodo.jpg)
> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

For detailed explanation on how things work, consult the [docs for vue-loader](http://vuejs.github.io/vue-loader).
